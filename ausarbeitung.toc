\select@language {american}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Background}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}MeSch EU Project}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}MeSchup Platform}{6}{section.2.2}
\contentsline {chapter}{\numberline {3}Related Work}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Ubiquitous Displays}{7}{section.3.1}
\contentsline {section}{\numberline {3.2}Interactivity through sensors and actuators}{9}{section.3.2}
\contentsline {section}{\numberline {3.3}Content delivery}{9}{section.3.3}
\contentsline {chapter}{\numberline {4}Concept}{11}{chapter.4}
\contentsline {section}{\numberline {4.1}Reducing technological complexity}{12}{section.4.1}
\contentsline {section}{\numberline {4.2}Content Creation}{12}{section.4.2}
\contentsline {section}{\numberline {4.3}On-Site Editing}{12}{section.4.3}
\contentsline {section}{\numberline {4.4}Interactivity Scripting}{13}{section.4.4}
\contentsline {section}{\numberline {4.5}Scenarios}{13}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Scenario I - Cultural Heritage}{13}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Scenario II - Retail Store}{17}{subsection.4.5.2}
\contentsline {subsection}{\numberline {4.5.3}Scenario III - Home}{18}{subsection.4.5.3}
\contentsline {chapter}{\numberline {5}Implementation}{21}{chapter.5}
\contentsline {section}{\numberline {5.1}Architecture}{21}{section.5.1}
\contentsline {section}{\numberline {5.2}User interface for creating interactive installations}{23}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Settings}{24}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Content Management}{25}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Project Management}{26}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Slide Editor}{27}{subsection.5.2.4}
\contentsline {subsection}{\numberline {5.2.5}Rule Editor}{27}{subsection.5.2.5}
\contentsline {section}{\numberline {5.3}Display client running on target device}{28}{section.5.3}
\contentsline {section}{\numberline {5.4}Deployment}{28}{section.5.4}
\contentsline {section}{\numberline {5.5}Project Modes}{29}{section.5.5}
\contentsline {section}{\numberline {5.6}On-Site Editing Approach}{29}{section.5.6}
\contentsline {section}{\numberline {5.7}Interactivity Scripting Approach}{31}{section.5.7}
\contentsline {section}{\numberline {5.8}Summary and Discussion}{33}{section.5.8}
\contentsline {chapter}{\numberline {6}Evaluation}{35}{chapter.6}
\contentsline {section}{\numberline {6.1}Case Study I - Pre Study}{35}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Participants and Setup}{35}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Procedure and Tasks}{36}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}Study results and discussion}{37}{subsection.6.1.3}
\contentsline {subsubsection}{\numberline {6.1.3.1}Creation strategies}{37}{subsubsection.6.1.3.1}
\contentsline {subsubsection}{\numberline {6.1.3.2}Working with the system}{38}{subsubsection.6.1.3.2}
\contentsline {subsubsection}{\numberline {6.1.3.3}Feedback}{40}{subsubsection.6.1.3.3}
\contentsline {subsubsection}{\numberline {6.1.3.4}Improvements}{41}{subsubsection.6.1.3.4}
\contentsline {section}{\numberline {6.2}Case Study II - Cultural Heritage}{42}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Participants and Setup}{42}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Procedure and Tasks}{43}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}Study results and discussion}{45}{subsection.6.2.3}
\contentsline {subsubsection}{\numberline {6.2.3.1}Creation strategies}{45}{subsubsection.6.2.3.1}
\contentsline {subsubsection}{\numberline {6.2.3.2}Working with the system}{46}{subsubsection.6.2.3.2}
\contentsline {section}{\numberline {6.3}Summary}{48}{section.6.3}
\contentsline {chapter}{\numberline {7}Discussion}{51}{chapter.7}
\contentsline {chapter}{\numberline {8}Conclusion and Future Work}{53}{chapter.8}
\contentsline {chapter}{\nonumberline Appendix}{57}{appendix*.33}
\contentsline {section}{\numberline {.1}Database Model}{58}{section.Alph0.1}
\contentsline {section}{\numberline {.2}Consent Form}{59}{section.Alph0.2}
\contentsline {section}{\numberline {.3}Gerneral Background Questionnaire}{60}{section.Alph0.3}
\contentsline {section}{\numberline {.4}System Usablity Scale Questionnaire}{62}{section.Alph0.4}
\contentsline {chapter}{\nonumberline Bibliography}{65}{appendix*.34}
